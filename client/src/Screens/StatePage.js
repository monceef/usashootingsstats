import React, { Component, Fragment } from "react";
import NavBar from "../Components/NavBar";
import StatsTabs from "../Components/StatsTabs";
import { Typography } from "@material-ui/core";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import StateList from "../Components/StateList";
import { getStateData } from "../actions/stateActions";

class StatePage extends Component {
  componentWillMount() {
    this.props.getStateData(this.props.match.params.stateID);
  }
  render() {
    return (
      <div className="App">
        {this.props.stateData === undefined ? (
          "Loading"
        ) : (
          <Fragment>
            <NavBar back history={this.props.history} />
            <div
              style={{
                padding: "24",
                flexDirection: "row",
                justifyContent: "space-between",
                display: "flex"
              }}
            />
            <div
              style={{
                alignItems: "center",
                display: "flex",
                flexDirection: "row",
                width: "20%",
                justifyContent: "space-around",
                marginTop: 24,
                marginBottom: 24
              }}
            >
              <img width={120} height={64} src={this.props.stateData.Image} />
              <div>
                <Typography style={{ fontSize: 16, fontWeight: "550" }}>
                  {this.props.match.params.stateID}
                </Typography>
                <Typography style={{ fontSize: 12, fontWeight: "550" }}>
                  {this.props.stateData.capital_city}
                </Typography>
                <a href="#" style={{ fontSize: 12, fontWeight: "550" }}>
                  {this.props.stateData.website}
                </a>
              </div>
            </div>
            <StatsTabs data={this.props.stateData} />
          </Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  stateData: state.statesReducer.stateData
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getStateData }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatePage);
