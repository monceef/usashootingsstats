// import { RECEIVE_USER_DATA, RECEIVE_USERS_DATA } from "../actions/Users";

const INTIAL_STATE = {
  bestStates: [],
  statesLoaded: false,
  mostDangerous: []
};

function statesReducer(state = INTIAL_STATE, action) {
  switch (action.type) {
    case "RECEIVE_BEST_STATES_DATA":
      return {
        ...state,
        bestStates: action.data,
        statesLoaded: true
      };

    case "RECEIVE_MOST_DANGEROUS_DATA":
      return {
        ...state,
        mostDangerous: action.data,
        statesLoaded: true
      };
    case "RECEIVE_STATE_DATA":
      return {
        ...state,
        stateData: action.data[0]
      };
    case "LOADING_DATA":
      return {
        ...state,
        statesLoaded: true
      };
    default:
      return state;
  }
}

export default statesReducer;
