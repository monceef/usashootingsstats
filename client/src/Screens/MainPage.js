import React, { Component } from "react";
import NavBar from "../Components/NavBar";
import StatsTabs from "../Components/StatsTabs";
import { Typography } from "@material-ui/core";
import StateList from "../Components/StateList";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getBestStates, getMostDangerous } from "../actions/stateActions";

class MainPage extends Component {
  componentWillMount() {
    this.props.getBestStates();
    this.props.getMostDangerous();
  }
  render() {
    return (
      <div className="App">
        <NavBar />
        <div
          style={{
            padding: "10%",
            flexDirection: "row",
            justifyContent: "space-between",
            display: "flex"
          }}
        >
          <div style={{ flex: 0.5 }}>
            <StateList
              data={this.props.bestStatesdata}
              history={this.props.history}
              title="Safest States"
            />
          </div>
          <div style={{ flex: 0.5 }}>
            <StateList
              data={this.props.mostDangerousStates}
              history={this.props.history}
              title="Dangerous States"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  bestStatesdata: state.statesReducer.bestStates,
  mostDangerousStates: state.statesReducer.mostDangerous,
  status: state.statesReducer.statesLoaded
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getBestStates, getMostDangerous }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);
