"use strict";

module.exports = function(State) {
  State.remoteMethod("GetBestStates", {
    accepts: [],
    http: {
      verb: "get",
      path: "/GetBestStates"
    },
    returns: {
      arg: "BestStates",
      type: "object"
    }
  });

  State.remoteMethod("GetMostDangerous", {
    accepts: [],
    http: {
      verb: "get",
      path: "/GetMostDangerous"
    },
    returns: {
      arg: "MostDangerous",
      type: "object"
    }
  });

  State.remoteMethod("GetStateData", {
    accepts: [
      {
        arg: "State",
        type: "String",
        required: true
      }
    ],
    http: {
      verb: "get",
      path: "/GetStateData"
    },
    returns: {
      arg: "StateData",
      type: "object"
    }
  });

  State.GetBestStates = function() {
    const sql =
      "SELECT * From State s ,lifestyle l Where l.State = s.Name ORDER BY l.Rank ASC LIMIT 10";
    const params = [];
    return new Promise((resolve, reject) => {
      this.dataSource.connector.execute(sql, params, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
  };

  State.GetMostDangerous = function() {
    const sql =
      "SELECT * From State s ,crimestats c Where c.State = s.Name ORDER BY c.NOFShootings DESC LIMIT 10";
    const params = [];
    return new Promise((resolve, reject) => {
      this.dataSource.connector.execute(sql, params, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
  };

  State.GetStateData = function(state) {
    const sql =
      "SELECT s.id, s.Image,s.website,s.capital_city,s.population,l.Rank,l.Index,l.Grocery,l.Housing,l.Transportation,l.Health,l.Income,c.NOFShootings,c.SchoolRelated,c.NoneRelated FROM state s, lifestyle l, crimestats c WHERE s.Name ='" +
      state +
      "' AND c.State='" +
      state +
      "' AND l.State='" +
      state +
      "'";
    const params = [state];
    return new Promise((resolve, reject) => {
      this.dataSource.connector.execute(sql, params, (err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
    });
  };
};
