import React, { Component, ScrollView } from "react";
import { Typography } from "@material-ui/core";
import State from "./State";

export default class StateList extends Component {
  render() {
    const { title } = this.props;
    return (
      <div>
        <Typography variant="display1" gutterBottom>
          {title}
        </Typography>
        <div
          style={{
            flexWrap: "wrap",
            display: "inline-flex",
            justifyContent: "space-evenly"
          }}
        >
          {this.props.data.map((state, index) => (
            <State key={index} history={this.props.history} state={state} />
          ))}
        </div>
      </div>
    );
  }
}
