import React, { Component } from "react";
import { Typography } from "@material-ui/core";

export default class LifeStyleStats extends Component {
  render() {
    const { data } = this.props;
    console.log(data);
    return (
      <div style={{ padding: 20 }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Population</Typography>
          <Typography>{data.population}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Rank</Typography>
          <Typography>{data.Rank}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Income</Typography>
          <Typography>{data.Income}</Typography>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Index</Typography>
          <Typography>{data.Index}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Grocery</Typography>
          <Typography>{data.Grocery}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Housing</Typography>
          <Typography>{data.Housing}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Transportation</Typography>
          <Typography>{data.Transportation}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Health</Typography>
          <Typography>{data.Health}</Typography>
        </div>
      </div>
    );
  }
}
