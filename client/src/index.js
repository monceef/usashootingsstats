import React from "react";
import ReactDOM from "react-dom";
import { Route, Router, Switch } from "react-router";
import { Provider } from "react-redux";
import Store from "./store";

import { createBrowserHistory } from "history";
import registerServiceWorker from "./registerServiceWorker";
import MainPage from "./Screens/MainPage";
import StatePage from "./Screens/StatePage";
const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={Store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route exact path="/State/:stateID" component={StatePage} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
