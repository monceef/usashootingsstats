"use strict";

var loopback = require("loopback");
var boot = require("loopback-boot");
const http = require("http");
var stateData = require("./data/states.json");
var app = (module.exports = loopback());
var axios = require("axios");

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit("started");
    var baseUrl = app.get("url").replace(/\/$/, "");
    console.log("Web server listening at: %s", baseUrl);
    if (app.get("loopback-component-explorer")) {
      var explorerPath = app.get("loopback-component-explorer").mountPath;
      console.log("Browse your REST API at %s%s", baseUrl, explorerPath);
    }
  });
};

// method to populate the db -- Should be customized later
app.get("/PopulateStates", function(req, res) {
  stateData.forEach(element => {
    axios
      .post(`http://localhost:3000/api/States`, {
        Name: element.state,
        Image: element.state_flag_url,
        website: element.website,
        capital_city: element.capital_city,
        population: element.population
      })
      .then(response => {
        res.status(200).json(response);
      })
      .catch(error => {
        console.log(error);
      });
  });
});

// method to populate the db -- Should be customized later
app.get("/PopulateLifestyle", function(req, res) {
  axios
    .get(
      "http://public.opendatasoft.com/api/records/1.0/search/?dataset=mass-shootings-in-america&facet=state",
      {}
    )
    .then(response => {
      console.log(response);
      res.status(200).json(response.data.facet_groups);
    })
    .catch(error => {
      console.log(error);
    });
});

// method to populate the db -- Should be customized later
app.get("/PopulateShootingsDB", function(req, res) {
  axios
    .get(
      "https://public.opendatasoft.com/api/records/1.0/search/?dataset=mass-shootings-in-america&rows=50&facet=state",
      {}
    )
    .then(response => {
      response.data.facet_groups.map(value => {
        value.facets.map(element => {
          var data = {};
          data.name = element.name;
          data.count = element.count;
          axios
            .get(
              "https://public.opendatasoft.com/api/records/1.0/search/?dataset=mass-shootings-in-america&rows=50&facet=school_related&refine.state=" +
                data.name,
              {}
            )
            .then(response => {
              let temp = response.data.facet_groups[0].facets.find(
                unit => unit.name === "Yes"
              );
              let tempNo = response.data.facet_groups[0].facets.find(
                unit => unit.name === "No"
              );
              if (temp === undefined) {
                data.schoolRelated = "0";
              } else {
                data.schoolRelated = temp.count;
              }
              if (tempNo === undefined) {
                data.noneRelated = "0";
              } else {
                data.noneRelated = tempNo.count;
              }
            })
            .then(() => {
              axios
                .post(`http://localhost:3000/api/CrimeStats`, {
                  State: data.name,
                  NOFShootings: data.count,
                  SchoolRelated: data.schoolRelated,
                  NoneRelated: data.noneRelated
                })
                .catch(error => {
                  console.log(error);
                });
            })
            .catch(error => {
              console.log(error);
            });
        });
      });

      res.status(200).json(response.data.facet_groups);
    })
    .catch(error => {
      console.log(error);
    });
});
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) app.start();
});
