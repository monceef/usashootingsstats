var server = require("./server");
var ds = server.dataSources.mySQL;
var lbTables = ["State", "Lifestyle", "CrimeStats", "Ratings"];
ds.autoupdate(lbTables, function(er) {
  if (er) throw er;
  ds.disconnect();
});
