import React, { Component } from "react";
import { Typography } from "@material-ui/core";

export default class State extends Component {
  render() {
    const { state } = this.props;
    return (
      <div
        style={{
          height: 115,
          width: 190,
          marginBottom: 12,
          alignItems: "center",
          justifyContent: "center",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          display: "flex",
          backgroundImage: `url(${state.Image})`
        }}
        onClick={() => this.props.history.push("/State/" + state.Name)}
      >
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0, 0, 0, 0.5)"
          }}
        >
          <Typography
            style={{
              color: "white",
              fontWeight: "600",
              fontFamily: "Baumans,cursive"
            }}
          >
            {state.Name}
          </Typography>
        </div>
      </div>
    );
  }
}
