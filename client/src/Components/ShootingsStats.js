import React, { Component } from "react";
import { Typography } from "@material-ui/core";

export default class ShootingsStats extends Component {
  render() {
    const { data } = this.props;
    return (
      <div style={{ padding: 20 }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Number Of Shootings</Typography>
          <Typography>{data.NOFShootings}</Typography>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>School Related</Typography>
          <Typography>{data.SchoolRelated}</Typography>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Typography>Unrelated</Typography>
          <Typography>{data.NoneRelated}</Typography>
        </div>
      </div>
    );
  }
}
