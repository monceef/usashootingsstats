import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import LifeStyleStats from "./LifeStyleStats";
import ShootingsStats from "./ShootingsStats";

class StatsTabs extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="LifeStyle Stats" />
          <Tab label="Shootings Stats" />
          <Tab label="User Reviews" />
        </Tabs>

        {value === 0 && <LifeStyleStats data={this.props.data} />}
        {value === 1 && <ShootingsStats data={this.props.data} />}
        {value === 2 && "Soon"}
      </Paper>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1,
    marginLeft: "12%",
    marginRight: "12%"
  }
};

export default withStyles(styles)(StatsTabs);
