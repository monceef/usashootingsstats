import axios from "axios";

export function getBestStates() {
  return function(dispatch) {
    return axios
      .get("http://localhost:3000/api/States/GetBestStates")
      .then(data => dispatch(receiveBestStates(data.data.BestStates)))
      .catch(error => {
        console.log(error);
      });
  };
}

export const receiveBestStates = data => ({
  type: "RECEIVE_BEST_STATES_DATA",
  data
});

export function getMostDangerous() {
  return function(dispatch) {
    return axios
      .get("http://localhost:3000/api/States/GetMostDangerous")
      .then(data => dispatch(receiveMostDangerous(data.data.MostDangerous)))
      .catch(error => {
        console.log(error);
      });
  };
}

export function getStateData(state) {
  return function(dispatch) {
    return axios
      .get("http://localhost:3000/api/States/GetStateData?State=" + state)
      .then(data => dispatch(receiveStateData(data.data.StateData)))
      .catch(error => {
        console.log(error);
      });
  };
}
export const receiveMostDangerous = data => ({
  type: "RECEIVE_MOST_DANGEROUS_DATA",
  data
});

export const receiveStateData = data => ({
  type: "RECEIVE_STATE_DATA",
  data
});
