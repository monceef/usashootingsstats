import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Back from "@material-ui/icons/ArrowBack";

class NavBar extends Component {
  render() {
    const { classes } = this.props;
    const { back, history } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            {back ? (
              <IconButton onClick={() => history.goBack()}>
                <Back />
              </IconButton>
            ) : (
              <div />
            )}
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
            >
              USA Shootings
            </Typography>
            <Button color="inherit">Register</Button>
            <Button color="inherit">Login</Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

export default withStyles(styles)(NavBar);
